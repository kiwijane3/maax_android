package maax.maaxandroid;

import android.app.Dialog;
import android.content.Context;

import maax.maaxandroid.controllers.ProductActionEditController;
import maax.maaxandroid.sdk.FeedClient;
import maax.maaxandroid.sdk.models.ProductAction;

/**
 * Created by janef on 20/01/17.
 */

public class Global {

    public static void showProductActionDialog(Context context, FeedClient client, ProductAction productAction){
        Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.product_action_dialog);
        ProductActionEditController controller = new ProductActionEditController(context, client, dialog);
        controller.setView(dialog.findViewById(R.id.root));
        controller.setContents(productAction);
    }

}
