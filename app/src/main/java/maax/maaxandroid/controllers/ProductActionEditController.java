package maax.maaxandroid.controllers;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.icu.util.TimeZone;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.triggertrap.seekarc.SeekArc;

import java.util.Calendar;
import java.util.GregorianCalendar;

import kiwijane3.generic.Event;
import kiwijane3.generic.Selection;
import kiwijane3.generic.ViewController;
import kiwijane3.generic.tasks.GenericTask;
import maax.maaxandroid.Format;
import maax.maaxandroid.R;
import maax.maaxandroid.adapters.ProductSuggestionAdapter;
import maax.maaxandroid.sdk.FeedClient;
import maax.maaxandroid.sdk.models.ProductAction;

/**
 * Created by janef on 19/01/17.
 */

public class ProductActionEditController extends ViewController<ProductAction> {

    private FeedClient client;

    private Dialog dialog;

    public ProductActionEditController(Context context, FeedClient inClient, Dialog inDialog){
        super(context);
        client = inClient;
        dialog = inDialog;
    }

    @Override
    protected void onBindView(ProductAction contents) {

        SeekArc discountArc = (SeekArc) findView(R.id.seekbar);
        discountArc.setArcRotation((int)(contents.getDiscount() * (discountArc.getSweepAngle() - discountArc.getStartAngle())));
        discountArc.setOnSeekArcChangeListener(new SeekArc.OnSeekArcChangeListener() {
            @Override
            public void onProgressChanged(SeekArc seekArc, int i, boolean b) {
                contents.setDiscount(i / (seekArc.getSweepAngle() - seekArc.getStartAngle()));
                ((TextView) findView(R.id.text_price)).setText(Format.doubleToPriceString(contents.getPrice()));
            }

            @Override
            public void onStartTrackingTouch(SeekArc seekArc) {

            }

            @Override
            public void onStopTrackingTouch(SeekArc seekArc) {

            }
        });

        ((TextView) findView(R.id.text_price)).setText(Format.doubleToPriceString(contents.getPrice()));

        AutoCompleteTextView productEdit = (AutoCompleteTextView) findView(R.id.edit_text_product);
        productEdit.setText(contents.getProductName());
        productEdit.setAdapter(new ProductSuggestionAdapter(context, client));

        ExpandableSelectorController selectorController = new ExpandableSelectorController(context);
        Selection locationSelection = client.locations.clone();
        locationSelection.onSelectionChanged.addListeners(() -> {
            contents.setLocations(locationSelection.getSelected());
        });
        selectorController.setContents(locationSelection);
        selectorController.setView((ViewSwitcher) findView(R.id.location_selector_switcher));
        ((TextView) findView(R.id.text_date)).setText(Format.calendarToFancyDateString(contents.getTimeStarts()));

        Calendar timeStarts = contents.getTimeStarts();
        findView(R.id.action_select_time).setOnClickListener((view) -> {
            new DatePickerDialog(
                    context,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            contents.setTimeStarts(new GregorianCalendar(year, month, dayOfMonth));
                            ((TextView) findView(R.id.text_date)).setText(Format.calendarToFancyDateString(contents.getTimeStarts()));
                        }
                    },
                    timeStarts.get(Calendar.YEAR),
                    timeStarts.get(Calendar.MONTH),
                    timeStarts.get(Calendar.DAY_OF_MONTH)
            ).show();
        });

        findView(R.id.action_cancel).setOnClickListener((view) -> {
            dialog.dismiss();
        });
        findView(R.id.action_confirm).setOnClickListener((view) -> {
            new GenericTask<ProductAction, Boolean>(
                    (action) -> {
                        return client.submitAction(action);
                    },
                    (success) -> {
                        int messageResource;
                        if (success){
                            messageResource = R.string.implement_action_success;
                        } else {
                            messageResource = R.string.implement_action_failure;
                        }
                        Snackbar.make(null, messageResource, Snackbar.LENGTH_LONG);
                    }
            ).execute(contents);
        });

        dialog.show();

    }
}
