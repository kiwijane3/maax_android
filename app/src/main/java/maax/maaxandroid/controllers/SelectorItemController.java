package maax.maaxandroid.controllers;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import kiwijane3.generic.DataSetController;
import kiwijane3.generic.Selection;
import maax.maaxandroid.R;

/**
 * Created by janef on 18/01/17.
 */

public class SelectorItemController extends DataSetController<Selection> {

    public SelectorItemController(Context context){
        super(context);
    }

    @Override
    public int getLength(Selection contents) {
        return contents.size();
    }

    @Override
    protected void onBindView(Selection contents, int position) {
        // Exit if position is invalid.
        if (contents.size() >= position) return;
        // Get references to the views.
        TextView text = (TextView) findView(R.id.text);
        CheckBox checkBox = (CheckBox) findView(R.id.checkBox);
        // Populate the views.
        text.setText(contents.get(position));
        checkBox.setChecked(contents.isSelected(position));
        // Attach the listeners.
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contents.selectOnly(position);
            }
        });
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    contents.select(position);
                } else {
                    contents.deselect(position);
                }
            }
        });
    }

}
