package maax.maaxandroid.controllers;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.widget.ImageView;
import android.widget.TextView;

import java.net.URISyntaxException;

import kiwijane3.generic.Event;
import kiwijane3.generic.tasks.GenericTask;
import maax.maaxandroid.Format;
import kiwijane3.generic.tasks.NetImageTask;
import kiwijane3.generic.ViewController;
import maax.maaxandroid.Global;
import maax.maaxandroid.R;
import maax.maaxandroid.sdk.FeedClient;
import maax.maaxandroid.sdk.models.ProductAction;

/**
 * Created by janef on 13/01/17.
 */

public class ProductActionController extends ViewController<ProductAction> {

    private FeedClient client;

    // The
    private ProductAction initialProductAction;

    public ProductActionController(Context context, FeedClient inClient){
        super(context);
        client = inClient;
    }

    @Override
    protected void onBindView(ProductAction contents) {
        ((TextView) findView(R.id.text_product)).setText(contents.getProductName());
        ((TextView) findView(R.id.text_price)).setText(Format.doubleToPriceString(contents.getPrice()));
        try {
            new NetImageTask((ImageView) findView(R.id.image)).execute(contents.getImage().toURI());
        } catch (URISyntaxException e) {
        }
        ((TextView) findView(R.id.text_location)).setText(Format.stringListToStringWithLimit(contents.getLocations(), 2));
        ((TextView) findView(R.id.text_time)).setText(Format.calendarToFancyString(contents.getTimeStarts()));
        findView(R.id.action_confirm).setOnClickListener((view) -> {
            new GenericTask<ProductAction, Boolean>(
                    (action) -> {
                        return client.submitAction(action);
                    },
                    (success) -> {
                        int messageResource;
                        if (success){
                            messageResource = R.string.implement_action_success;
                        } else {
                            messageResource = R.string.implement_action_failure;
                        }
                        Snackbar.make(null, messageResource, Snackbar.LENGTH_LONG);
                    }
            ).execute(contents);
        });
        findView(R.id.action_dismiss).setOnClickListener((view) -> {
            //TODO remove item.
        });
        findView(R.id.action_edit).setOnClickListener((view) -> {
            Global.showProductActionDialog(context, client, contents);
        });
    }

}
