package maax.maaxandroid.controllers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import kiwijane3.generic.Selection;
import kiwijane3.generic.ViewController;
import maax.maaxandroid.Format;
import maax.maaxandroid.R;
import maax.maaxandroid.adapters.SelectorAdapter;

/**
 * Created by janef on 18/01/17.
 */

public class ExpandableSelectorController extends ViewController<Selection> {

    public ExpandableSelectorController(Context context){
        super(context);
    }

    @Override
    protected void onBindView(Selection contents){
        findView(R.id.location_selector_collapsed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewSwitcher) findView(R.id.location_selector_switcher)).showNext();
            }
        });
        findView(R.id.location_selector_collapse).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewSwitcher) findView(R.id.location_selector_collapse)).showNext();
            }
        });
        ((RecyclerView) findView(R.id.location_selector_list)).setAdapter(new SelectorAdapter(context, contents));
        contents.onSelectionChanged.addListeners(() -> {
            TextView summary = (TextView) findView(R.id.text_location_summary);
            if (summary != null){
                summary.setText(Format.stringListToStringWithLimit(contents.getSelected(), 2));
            }
        });

    }

}