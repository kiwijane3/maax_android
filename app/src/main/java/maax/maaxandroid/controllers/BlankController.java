package maax.maaxandroid.controllers;

import android.content.Context;

import kiwijane3.generic.ViewController;
import maax.maaxandroid.sdk.models.MaaxItem;

/**
 * Created by janef on 14/01/17.
 */
// A ViewController that does nothing.
public class BlankController extends ViewController<MaaxItem> {

    public BlankController(Context context){
        super(context);
    }

    @Override
    protected void onBindView(MaaxItem contents) {
        // NOP
    }
}
