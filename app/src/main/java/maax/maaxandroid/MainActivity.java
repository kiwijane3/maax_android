package maax.maaxandroid;

import android.app.Activity;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ViewSwitcher;

import org.json.JSONException;

import java.io.IOException;

import kiwijane3.generic.ActivityViewCache;
import kiwijane3.generic.tasks.GenericTask;
import kiwijane3.generic.tasks.GenericWeakTask;
import maax.maaxandroid.adapters.FeedAdapter;
import maax.maaxandroid.adapters.SelectorAdapter;
import maax.maaxandroid.controllers.ExpandableSelectorController;
import maax.maaxandroid.sdk.FeedClient;
import maax.maaxandroid.sdk.models.ProductAction;

public class MainActivity extends AppCompatActivity {

    private ActivityViewCache cache;

    private FeedClient client;

    private View findView(int id){
        return cache.findView(id);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cache = new ActivityViewCache(this);

        Toolbar toolbar = (Toolbar) findView(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        DrawerLayout drawerLayout = (DrawerLayout) findView(R.id.root);
        drawerLayout.addDrawerListener(new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_closed){

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // If the feed has been invalidated, revalidate it and notify the adapter.
                if (!client.isValid()){
                    new GenericTask<Void, Boolean>(
                            (a) -> {
                                try {
                                    client.revalidate();
                                    return true;
                                } catch (IOException e) {
                                    return false;
                                } catch (JSONException e) {
                                    return false;
                                }
                            },
                            (success) -> {
                                ((RecyclerView) findView(R.id.feed)).getAdapter().notifyDataSetChanged();
                            }
                    ).execute();
                }
            }
        });

        Activity self = this;

        new GenericWeakTask<Void, FeedClient, MainActivity>(
                this,
                (a) -> {
                    try {
                        return new FeedClient();
                    } catch (IOException e) {
                        return null;
                    } catch (JSONException e) {
                        return null;
                    }
                },
                (activity, inClient) -> {
                    activity.client = inClient;
                    ((RecyclerView) activity.findView(R.id.feed)).setAdapter(new FeedAdapter(self, client));

                    ViewSwitcher locationSwitcher = (ViewSwitcher) activity.findView(R.id.location_selector_switcher);
                    ExpandableSelectorController locationController = new ExpandableSelectorController(activity);
                    locationController.setView(locationSwitcher);
                    locationController.setContents(activity.client.locations);

                    RecyclerView typeSelector = (RecyclerView) activity.findView(R.id.type_selector);
                    typeSelector.setAdapter(new SelectorAdapter(activity, activity.client.types));

                    FloatingActionButton button = (FloatingActionButton) activity.findView(R.id.action_custom);
                    button.setOnClickListener((view) -> {
                        Global.showProductActionDialog(activity, client, new ProductAction());
                    });

                }
        ).execute();

    }
}