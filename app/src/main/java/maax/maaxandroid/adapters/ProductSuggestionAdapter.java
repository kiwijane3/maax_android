package maax.maaxandroid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import maax.maaxandroid.R;
import maax.maaxandroid.sdk.FeedClient;

import static java.util.Collections.EMPTY_LIST;

/**
 * Created by janef on 20/01/17.
 */

public class ProductSuggestionAdapter extends BaseAdapter implements Filterable {

    private static final List<String> EMPTY_LIST = new ArrayList<>();

    private Context context;

    private List<String> data;

    private FeedClient client;

    public ProductSuggestionAdapter(Context inContext, FeedClient inClient){
        context = inContext;
        data = EMPTY_LIST;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.text_suggestion, parent, true);
        }
        ((TextView) convertView.findViewById(R.id.text)).setText(data.get(position));
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if (constraint != null){
                    List<String> products = client.getProducts(constraint);
                    results.values = products;
                    results.count = products.size();
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0){
                    data = (List<String>) results.values;
                    notifyDataSetChanged();
                } else {
                    data = EMPTY_LIST;
                    notifyDataSetInvalidated();
                }
            }
        };
    }

}
