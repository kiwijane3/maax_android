package maax.maaxandroid.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import org.json.JSONException;

import java.io.IOException;

import kiwijane3.generic.ControllerViewHolder;
import kiwijane3.generic.tasks.GenericTask;
import maax.maaxandroid.controllers.ProductActionController;
import maax.maaxandroid.R;
import maax.maaxandroid.controllers.BlankController;
import maax.maaxandroid.sdk.FeedClient;
import maax.maaxandroid.sdk.models.MaaxItem;
import maax.maaxandroid.sdk.models.ProductAction;

/**
 * Created by janef on 14/01/17.
 */

public class FeedAdapter extends RecyclerView.Adapter<ControllerViewHolder<? extends MaaxItem>> {

    private static final int TYPE_PRODUCT_ACTION = 0;

    private static final int TYPE_ANY = -1;

    private Context context;

    private FeedClient client;

    public FeedAdapter(Context inContext, FeedClient inClient){
        context = inContext;
        client = inClient;
    }

    @Override
    public ControllerViewHolder<? extends MaaxItem> onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_PRODUCT_ACTION){
            return new ProductActionViewHolder(context, parent, client);
        } else {
            return new BlankViewHolder(context, parent);
        }
    }

    @Override
    public void onBindViewHolder(ControllerViewHolder<? extends MaaxItem> holder, int position) {
        MaaxItem item = client.getItem(position);
        if (item instanceof ProductAction){
            ((ProductActionViewHolder) holder).setContents((ProductAction) item);
        } else {
            //NOP
        }
        // If this is the last position, try to load more items from the server.
        if (position == client.size() - 1){
            new GenericTask<Void, Boolean>(
                    (a) -> {
                        try {
                            client.getNext();
                            return true;
                        } catch (IOException e) {
                            return false;
                        } catch (JSONException e) {
                            return false;
                        }
                    },
                    (success) -> {
                        if (success) {
                            notifyDataSetChanged();
                        }
                    }
            ).execute();
        }
    }

    @Override
    public int getItemViewType(int position) {
        MaaxItem item = client.getItem(position);
        if (item instanceof ProductAction){
            return TYPE_PRODUCT_ACTION;
        } else {
            return TYPE_ANY;
        }
    }

    @Override
    public int getItemCount() {
        return client.size();
    }

    private class ProductActionViewHolder extends ControllerViewHolder<ProductAction>{

        protected ProductActionViewHolder(Context context, ViewGroup parent, FeedClient client) {
            super(LayoutInflater.from(context).inflate(R.layout.product_action_card_layout, parent, true), new ProductActionController(context, client));
        }
    }

    private class BlankViewHolder extends ControllerViewHolder<MaaxItem> {

        protected BlankViewHolder(Context context, ViewGroup parent){
            super(LayoutInflater.from(context).inflate(R.layout.blank, parent, true), new BlankController(context));
        }

    }

}
