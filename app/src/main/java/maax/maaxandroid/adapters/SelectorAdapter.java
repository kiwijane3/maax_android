package maax.maaxandroid.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import kiwijane3.generic.DataSetControllerViewHolder;
import kiwijane3.generic.Selection;
import maax.maaxandroid.R;
import maax.maaxandroid.controllers.SelectorItemController;

/**
 * Created by janef on 18/01/17.
 */

public class SelectorAdapter extends RecyclerView.Adapter<SelectorAdapter.SelectorItemHolder> {

    private Context context;

    private Selection selection;

    public SelectorAdapter(Context inContext, Selection inSelection){
        selection = inSelection;
        context = inContext;
        selection.onSelectionChanged.addListeners(() -> {
            notifyDataSetChanged();
        });
    }

    @Override
    public SelectorItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SelectorItemHolder(context, parent, selection);
    }

    @Override
    public void onBindViewHolder(SelectorItemHolder holder, int position) {
        holder.setPosition(position);
    }

    @Override
    public int getItemCount() {
        return selection.size();
    }

    public class SelectorItemHolder extends DataSetControllerViewHolder<Selection> {

        public SelectorItemHolder(Context context, ViewGroup parent, Selection selection){
            super(LayoutInflater.from(context).inflate(R.layout.list_item_selector, parent, true), new SelectorItemController(context));
            controller.setContents(selection);
        }

    }

}
