package maax.maaxandroid.sdk.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import kiwijane3.generic.Conversion;

/**
 * Created by janef on 13/01/17.
 */

public class ProductAction implements MaaxItem {

    private static final String KEY_PRODUCT_NAME = "productName";

    private String productName;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    private static final String KEY_LIST_PRICE = "listPrice";

    private double listPrice;

    public double getListPrice() {
        return listPrice;
    }

    public void setListPrice(double listPrice) {
        this.listPrice = listPrice;
    }

    private static final String KEY_DISCOUNT = "discount";

    private double discount;

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getPrice(){
        return (1 - discount) * listPrice;
    }

    private static final String KEY_TIME_STARTS = "timeStarts";

    private Calendar timeStarts;


    public Calendar getTimeStarts() {
        return timeStarts;
    }

    public void setTimeStarts(Calendar timeStarts) {
        this.timeStarts = timeStarts;
    }

    private static final String KEY_TIME_ENDS = "timeEnds";

    private Calendar timeEnds;

    public Calendar getTimeEnds() {
        return timeEnds;
    }

    public void setTimeEnds(Calendar timeEnds) {
        this.timeEnds = timeEnds;
    }

    private static final String KEY_LOCATIONS = "locations";

    private List<String> locations;

    public List<String> getLocations() {
        return locations;
    }

    public void setLocations(List<String> locations) {
        this.locations = locations;
    }

    private static final String KEY_IMAGE = "image";

    private URL image;

    public URL getImage() {
        return image;
    }

    public void setImage(URL image) {
        this.image = image;
    }

    public ProductAction(JSONObject json) throws JSONException, MalformedURLException{
        productName = json.getString(KEY_PRODUCT_NAME);
        listPrice = json.getDouble(KEY_LIST_PRICE);
        discount = json.getDouble(KEY_DISCOUNT);
        TimeZone timeZone = TimeZone.getDefault();
        timeStarts = new GregorianCalendar(timeZone);
        timeStarts.setTimeInMillis(json.getLong(KEY_TIME_STARTS));
        timeEnds = new GregorianCalendar(timeZone);
        timeEnds.setTimeInMillis(json.getLong(KEY_TIME_ENDS));
        locations = Conversion.jsonArrayToStringList(json.getJSONArray(KEY_LOCATIONS));
        image = new URL(json.getString(KEY_IMAGE));
    }

    public ProductAction(){
        productName = "";
        listPrice = 0;
        discount = 0;
        TimeZone timeZone = TimeZone.getDefault();
        timeStarts = new GregorianCalendar(timeZone);
        timeEnds = new GregorianCalendar(timeZone);
        locations = new ArrayList<>();
        try {
            image = new URL("http:/www.invalid.com");
        } catch (MalformedURLException murle){
        }
    }

    public String serialize(){
        try {
            JSONObject out = new JSONObject();
            out.put(KEY_PRODUCT_NAME, productName);
            out.put(KEY_LIST_PRICE, listPrice);
            out.put(KEY_DISCOUNT, discount);
            out.put(KEY_TIME_STARTS, timeStarts.getTimeInMillis());
            out.put(KEY_TIME_ENDS, timeEnds.getTimeInMillis());
            out.put(KEY_LOCATIONS, Conversion.stringListToJsonArray(locations));
            out.put(KEY_IMAGE, image.toExternalForm());
            return out.toString();
        } catch (JSONException jsone){
            return "";
        }
    }

    @Override
    public boolean equals(Object other){
        if (other instanceof ProductAction){
            return serialize().equals(((ProductAction) other).serialize());
        } else {
            return false;
        }
    }

}
