package maax.maaxandroid.sdk;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;


import io.reactivex.Observable;
import io.reactivex.Observer;
import kiwijane3.generic.Conversion;
import kiwijane3.generic.Event;
import kiwijane3.generic.Selection;
import maax.maaxandroid.Format;
import maax.maaxandroid.sdk.models.BlankItem;
import maax.maaxandroid.sdk.models.MaaxItem;
import maax.maaxandroid.sdk.models.ProductAction;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by janef on 13/01/17.
 */

public class FeedClient {

    private static final String API_URL = "http://api.placeholder.com";

    private static final String REQUEST_TEMPLATE = "%s/feed?offset=%d&limit=%d&types=%s&locations=%s&date=%d";

    private static int BLOCK_SIZE = 20;

    private OkHttpClient client;

    private List<MaaxItem> contents;

    public MaaxItem getItem(int index){
        return contents.get(index);
    }

    public int size(){
        return contents.size();
    }

    private boolean valid;

    public boolean isValid() {
        return valid;
    }

    public Selection locations;

    public Selection types;

    public Calendar time;

    //Update events

    public final Observable onUpdate;

    public FeedClient() throws IOException, JSONException{
        contents = new ArrayList<>();
        client = new OkHttpClient();
        Request locationsRequest = new Request.Builder().url(String.format("%s/locations", API_URL)).get().build();
        Response locationsResponse = client.newCall(locationsRequest).execute();
        List<String> locationsList = Conversion.jsonArrayToStringList(new JSONArray(locationsResponse.body().string()));
        locations = new Selection(locationsList);
        Request typesRequest = new Request.Builder().url(String.format("%s/types", API_URL)).get().build();
        Response typesResponse = client.newCall(locationsRequest).execute();
        List<String> typesList = Conversion.jsonArrayToStringList(new JSONArray(locationsResponse.body().string()));
        types = new Selection(typesList);
        // Add listeners to the selections in order to invalidate the feed.
        locations.onSelectionChanged.addListeners(() -> {
            valid = false;
        });
        types.onSelectionChanged.addListeners(() -> {
            valid = false;
        });
        time = new GregorianCalendar(TimeZone.getDefault());
        time.setTime(new Date());
        getNext();

        onUpdate = new Observable() {
            @Override
            protected void subscribeActual(Observer observer) {

            }
        };

    }

    public void revalidate() throws IOException, JSONException{
        // Clear contents.
        contents.removeAll(contents);
        // Get new contents.
        getNext();
    }

    public void getNext() throws IOException, JSONException{
        Request request = new Request.Builder().url(getRequestString(contents.size())).get().build();
        Response response = client.newCall(request).execute();
        if (response.code() != 200){
            throw new IOException("Unexpected response code");
        }
        JSONArray array = new JSONArray(response.body().string());
        for (int i = 0; i < array.length(); ++i){
            contents.add(readFeedItem(array.getJSONObject(i)));
        }
    }

    public String getRequestString(int offset){
        return String.format(REQUEST_TEMPLATE, API_URL, offset, BLOCK_SIZE, types.getSelectedAsCompactString(), locations.getSelectedAsCompactString(), time.getTimeInMillis());
    }

    private static final String KEY_TYPE = "type";
    private static final String KEY_CONTENTS =  "contents";

    private static final String ID_PRODUCT_ACTION = "PRODUCT_ACTION";

    public MaaxItem readFeedItem(JSONObject object){
        try {
            switch (object.getString(KEY_TYPE)) {
                case ID_PRODUCT_ACTION:
                    return new ProductAction(object.getJSONObject(KEY_CONTENTS));
                default:
                    return new BlankItem();
            }
        } catch (JSONException jsone){
            return new BlankItem();
        } catch (MalformedURLException murle) {
            return new BlankItem();
        }

    }

    private static final String SUBMIT_ACTION_TEMPLATE = "%s/action";

    private static final String MEDIA_TYPE_JSON = "application/json";

    /**
     * Submits the given action.
     * @param action
     * @return success
     */
    public boolean submitAction(ProductAction action){
        try {
            Request request = new Request.Builder()
                    .url(String.format(SUBMIT_ACTION_TEMPLATE, API_URL))
                    .post(RequestBody.create(MediaType.parse(MEDIA_TYPE_JSON), action.serialize()))
                    .build();
            Response response = client.newCall(request).execute();
            if (response.code() == 201){
                // TODO Modify state contents
                return true;
            } else {
                return false;
            }
        } catch (IOException ioe){
            return false;
        }
    }

    public void dismissAction(ProductAction action){
        // TODO Implement code to dismiss the action.
    }

    private static final String PRODUCT_TEMPLATE = "%s/products?=%s";

    public List<String> getProducts(CharSequence constraint){
        try {
            Request request = new Request.Builder().url(String.format(PRODUCT_TEMPLATE, API_URL, constraint)).build();
            Response response = client.newCall(request).execute();
            if (response.code() == 200){
                return Conversion.jsonArrayToStringList(new JSONArray(response.body().string()));
            } else {
                return new ArrayList<>();
            }
        } catch (IOException ioe){
            return new ArrayList<>();
        } catch (JSONException jsone){
            return new ArrayList<>();
        }
    }

}
