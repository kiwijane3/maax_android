package maax.maaxandroid;

import java.util.Calendar;
import java.util.List;

/**
 * Created by janef on 13/01/17.
 */

public class Format {

    public static String doubleToPriceString(double in){
        return String.format("$%.2f", in);
    }

    public static String stringListToStringWithLimit(List<String> list, int limit){
        String out = "";
        for (int i = 0; i < list.size() && i < limit; ++i){
            // Append string.
            out += list.get(i);
            // Add something if this isn't the last string in the list.
            if (i < (list.size() - 1)){
                // If no more items can be displayed, add "..."
                if (i == limit - 1){
                    out += "...";
                // Add ", " between items.
                } else {
                    out += ", ";
                }
            }
        }
        return out;
    }

    private static final String FANCY_TIME_TEMPLATE = "%d:%d, %s %d of %s";

    public static String calendarToFancyString(Calendar calendar){
        return String.format(FANCY_TIME_TEMPLATE, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),
                intToDayName(calendar.get(Calendar.DAY_OF_WEEK)), calendar.get(Calendar.DAY_OF_MONTH),
                intToMonthName(calendar.get(Calendar.MONTH)));
    }

    public static final String FANCY_DATE_TEMPLATE = "%s %d of %s";

    public static String calendarToFancyDateString(Calendar calendar){
        return String.format(FANCY_DATE_TEMPLATE, intToDayName(calendar.get(Calendar.DAY_OF_WEEK)),
                calendar.get(Calendar.DAY_OF_MONTH), intToMonthName(calendar.get(Calendar.MONTH)));
    }

    public static String intToDayName(int day){
        switch (day){
            case 1:
                return "Monday";
            case 2:
                return "Tuesday";
            case 3:
                return "Wednesday";
            case 4:
                return "Thursday";
            case 5:
                return "Friday";
            case 6:
                return "Saturday";
            case 7:
                return "Sunday";
            default:
                return "INVALID_DAY!";
        }
    }

    public static String intToMonthName(int month){
        switch (month){
            case 1:
                return "January";
            case 2:
                return "February";
            case 3:
                return "March";
            case 4:
                return "April";
            case 5:
                return "May";
            case 6:
                return "June";
            case 7:
                return "July";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "October";
            case 11:
                return "November";
            case 12:
                return "December";
            default:
                return "INVALID_MONTH!";
        }
    }

}
