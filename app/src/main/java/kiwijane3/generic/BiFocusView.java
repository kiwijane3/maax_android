package kiwijane3.generic;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import maax.maaxandroid.R;

public class BiFocusView extends FrameLayout {

    // The view to be displayed when the BiFocusView doesn't have focus.
    private View nonFocusView;
    private ViewGroup.LayoutParams nonFocusParams;

    //The view to be displayed when the BiFocusView does have focus.
    private View focusView;
    private ViewGroup.LayoutParams focusParams;

    public BiFocusView(Context context) {
        super(context);
    }

    public BiFocusView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BiFocusView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFocusChanged(boolean gainFocus, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);
    }

    @Override
    public void addView(View child) {
        if (nonFocusView == null){
            nonFocusView = child;
        } else if (focusView == null){
            focusView = child;
        }
        updateView();
    }

    @Override
    public void addView(View child, int index) {
        if (index == 0){
            nonFocusView = child;
        } else if (index == 1){
            focusView = child;
        }
        updateView();
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        if (index == 0){
            nonFocusView = child;
            nonFocusParams = params;
        } else if (index == 1){
            focusView = child;
            focusParams = params;
        }
        updateView();
    }

    private void updateView(){
        if (isFocused()){
            super.addView(focusView, 0, focusParams);
        } else {
            super.addView(nonFocusView, 0, nonFocusParams);
        }
    }
}
