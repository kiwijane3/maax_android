package kiwijane3.generic;

import android.os.Looper;

import kiwijane3.generic.functionalinterfaces.Action;

/**
 * Created by janef on 21/01/17.
 */

public class UIAction implements Action {

    private Action action;

    public UIAction(Action inAction){
        action = inAction;
    }

    public void invoke(){
    }

}
