package kiwijane3.generic;

/**
 * Created by janef on 18/01/17.
 */

import android.content.Context;

/**
 * A view controller where the contents is a collection of data and the Controller displays one item in them.
 */
public abstract class DataSetController<A> extends ViewController<A> {

    private int position;


    public DataSetController(Context context){
        super(context);
        position = 0;
    }

    /**
     * Sets position to the specified position.
     * @param inPosition
     */
    public void setPosition(int inPosition){
        position = inPosition;
        bindView();
    }

    @Override
    protected final void onBindView(A contents) {
        onBindView(contents, position);
    }

    /**
     * The binding logic, including the contents and the position. Use instead of onBindView(A contents).
     * @param contents
     * @param position
     */
    protected abstract void onBindView(A contents, int position);

    /**
     * Validates the position before binding the view.
     * @return
     */
    protected boolean onValidateBind(A contents){
        return 0 < position && position < getLength(contents);
    }

    /**
     * Returns the length of the collection. Required for validating position.
     */
    public abstract int getLength(A contents);

}
